
  $('.carousel').carousel({
  interval: 0
});


$('a[href*=#]:not([href=#media])').click(function () {
    var target = $(this.href);
    if( target.length ) {
        event.preventDefault();
       $('html, body').animate({
            scrollTop: target.offset().top
          }, 1000);
    }
});


$('p').each(function(){
    var string = $(this).html();
    string = string.replace(/ ([^ ]*)$/,'&nbsp;$1');
    $(this).html(string);
});


  
$(document).scroll(function() {
  var y = $(this).scrollTop();
  if (y > 200) {
    $('.bottomMenu').fadeIn();
  } else {
    $('.bottomMenu').fadeOut();
  }
});