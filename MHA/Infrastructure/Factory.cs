﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using MHA.Models;
using umbraco.dialogs;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;

namespace MHA.Infrastructure
{
    public class Factory
    {
        private readonly UmbracoHelper _publishedContent;
        private readonly IContentTypeService _contentTypeService;
        public Cache Cache;

        public static int HomeNodeId;

        public Factory()
        {
            _publishedContent = new UmbracoHelper(UmbracoContext.Current);
            _contentTypeService = ApplicationContext.Current.Services.ContentTypeService;
            Cache = HttpRuntime.Cache;
        }
        public HomeModel HomeModelBuilder(int homeNodeId)
        {
            CacheSiteId();
            var model = new HomeModel()
            {
                ImageBlock = new ImageBlock(),
                InformationBlock = new InformationBlock(),
                CarouselBlock = new CarouselBlock(),
                LocationBlock = new LocationBlock(),
                RegisterBlock = new RegisterBlock()
            };

            HomeNodeId = homeNodeId;
            BuildImageBlock(model);
            BuildInformationBlock(model);
            BuildCarouselBlock(model);
            BuildLocationBlock(model);
            BuildRegisterBlock(model);
            return model;
        }

        internal object NotFoundBuilder(int id)
        {
            throw new NotImplementedException();
        }

        public void CacheSiteId()
        {
            Cache.Remove("SiteId");
            Cache.Add("SiteId", HomeNodeId, null, DateTime.Now.AddDays(10),
                Cache.NoSlidingExpiration, CacheItemPriority.High, null);
        }

        public ThankYouModel ThankYouModelBuilder()
        {
            if (HttpRuntime.Cache.Get("SiteId") == null) return new ThankYouModel();
            var siteId = int.Parse(HttpRuntime.Cache.Get("SiteId").ToString());
            var homeNode = _publishedContent.TypedContent(siteId);
            var thankYouDocType = _contentTypeService.GetContentType("ThankYou");
            var thankYouBlockNode = homeNode.Children().First(node => node.ContentType.Alias == thankYouDocType.Alias);
            return new ThankYouModel()
            {
                InsertIntoPageHead = AssignStringValue(thankYouBlockNode, "insertIntoPageHead"),
                HomePageRedirect = homeNode.Url
            };
        }

        public HomeModel BuildImageBlock(HomeModel model)
        {
            var homeNode = _publishedContent.TypedContent(HomeNodeId);
            var imageBlockDocType = _contentTypeService.GetContentType("ImageBlock");
            var imageBlockNode = homeNode.Children().First(node => node.ContentType.Alias == imageBlockDocType.Alias);

            model.ImageBlock.MainImageUrl = GetImageMediaId(imageBlockNode, "mainImage");
            model.ImageBlock.BoxTitle = AssignStringValue(imageBlockNode, "boxTitle");
            model.ImageBlock.BoxText = AssignStringValue(imageBlockNode, "boxText");
            model.ImageBlock.ButtonText = AssignStringValue(imageBlockNode, "buttonText");
            return model;
        }

        public HomeModel BuildInformationBlock(HomeModel model)
        {
            var homeNode = _publishedContent.TypedContent(HomeNodeId);
            var informationBlockDocType = _contentTypeService.GetContentType("InformationBlock");
            var informationBlockNode = homeNode.Children().First(node => node.ContentType.Alias == informationBlockDocType.Alias);

            model.InformationBlock.Title = AssignStringValue(informationBlockNode, "title");
            model.InformationBlock.Subtitle = AssignStringValue(informationBlockNode, "subtitle");
            model.InformationBlock.Heading = AssignStringValue(informationBlockNode, "heading");
            model.InformationBlock.Text = AssignStringValue(informationBlockNode, "text");
            return model;
        }

        public HomeModel BuildCarouselBlock(HomeModel model)
        {
            var homeNode = _publishedContent.TypedContent(HomeNodeId);
            var carouselBlockDocType = _contentTypeService.GetContentType("CarouselBlock");
            var carouselBlockNode = homeNode.Children().First(node => node.ContentType.Alias == carouselBlockDocType.Alias);

            model.CarouselBlock.CarouselTitle = AssignStringValue(carouselBlockNode, "carouselTitle");
            var carouselItems = carouselBlockNode.Children().Select(item => new CarouselItem()
            {
                ItemImageUrl = GetImageMediaId(item, "itemImage"),
                ItemTitle = AssignStringValue(item, "itemTitle"),
                ItemText = AssignStringValue(item, "itemText")
            }).ToList();

            model.CarouselBlock.CarouselItems = carouselItems;
            return model;
        }

        public HomeModel BuildLocationBlock(HomeModel model)
        {
            var homeNode = _publishedContent.TypedContent(HomeNodeId);
            var locationBlockDocType = _contentTypeService.GetContentType("LocationBlock");
            var locationBlockNode = homeNode.Children().First(node => node.ContentType.Alias == locationBlockDocType.Alias);

            model.LocationBlock.LocationName = AssignStringValue(locationBlockNode, "locationName");
            model.LocationBlock.Subtitle = AssignStringValue(locationBlockNode, "subtitle");
            model.LocationBlock.Text = AssignStringValue(locationBlockNode, "text");
            model.LocationBlock.MapPath = GetImageMediaId(locationBlockNode, "map");
            model.LocationBlock.GoogleMapsUrl = AssignStringValue(locationBlockNode, "googleMapsUrl");
            return model;
        }

        public HomeModel BuildRegisterBlock(HomeModel model)
        {
            var homeNode = _publishedContent.TypedContent(HomeNodeId);
            var registerBlockDocType = _contentTypeService.GetContentType("RegisterBlock");
            var registerBlockNode = homeNode.Children().First(node => node.ContentType.Alias == registerBlockDocType.Alias);

            model.RegisterBlock.BoxTitle = AssignStringValue(registerBlockNode, "boxTitle");
            model.RegisterBlock.Title = AssignStringValue(registerBlockNode, "title");
            model.RegisterBlock.Text = AssignStringValue(registerBlockNode, "text");
            model.RegisterBlock.JotFormId = AssignStringValue(registerBlockNode, "jotFormId");
            return model;
        }

        public string AssignStringValue(IPublishedContent blockNode, string aliasName)
        {
            var resultString = "";
            if (blockNode.GetPropertyValue(aliasName) != null)
            {
                resultString = blockNode.GetPropertyValue(aliasName).ToString();
            }
            return resultString;
        }

        public string GetImageMediaId(IPublishedContent node, string aliasName)
        {
            var mediaId = node.GetPropertyValue(aliasName) != null ? int.Parse(node.GetPropertyValue(aliasName).ToString()) : 0;
            var mediaService = ApplicationContext.Current.Services.MediaService;
            var image = mediaService.GetById(mediaId);
            return image != null ? image.GetValue("umbracoFile").ToString() : "";
        }
    }
}