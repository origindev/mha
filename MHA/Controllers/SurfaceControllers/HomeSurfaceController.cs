﻿using System;
using System.Web.Mvc;
using MHA.Infrastructure;
using Umbraco.Web;

namespace MHA.Controllers.SurfaceControllers
{
    public class HomeSurfaceController : Umbraco.Web.Mvc.SurfaceController
    {
        public ActionResult RenderHome()
        {
            var factory = new Factory();
            var model = factory.HomeModelBuilder(CurrentPage.Id);
            return PartialView("HomePartial", model);
        }

        public ActionResult RenderThankYou()
        {
            var factory = new Factory();
            var model = factory.ThankYouModelBuilder();
            return View("ThankYou", model);
        }

        [ChildActionOnly]
        public ActionResult RenderNotFound()
        {
            var factory = new Factory();
            var model = factory.NotFoundBuilder(CurrentPage.Id);
            return PartialView("NotFound", model);
        }

    }
}