﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using System.Web;

namespace MHA.Models
{
    public class HomeModel
    {
        public ImageBlock ImageBlock { get; set; }
        public InformationBlock InformationBlock { get; set; }
        public CarouselBlock CarouselBlock { get; set; }
        public LocationBlock LocationBlock { get; set; }
        public RegisterBlock RegisterBlock { get; set; }
    }

    public class ImageBlock
    {
        public string MainImageUrl { get; set; }
        public string BoxTitle { get; set; }
        public string BoxText { get; set; }
        public string ButtonText { get; set; }
    }

    public class InformationBlock
    {
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string Heading { get; set; }
        public string Text { get; set; }
    }

    public class CarouselBlock
    {
        public string CarouselTitle { get; set; }
        public List<CarouselItem> CarouselItems { get; set; }
    }

    public class CarouselItem
    {
        public string ItemImageUrl { get; set; }
        public string ItemTitle { get; set; }
        public string ItemText { get; set; }
    }

    public class LocationBlock
    {
        public string LocationName { get; set; }
        public string Subtitle { get; set; }
        public string Text { get; set; }
        public string MapPath { get; set; }
        public string GoogleMapsUrl { get; set; }
    }

    public class RegisterBlock
    {
        public string BoxTitle { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public string JotFormId { get; set; }

    }

    public class ThankYouModel
    {
        public string InsertIntoPageHead { get; set; }
        public string HomePageRedirect { get; set; }
    }

    public class NotFound
    {
        public string Image { get; set; }
        public string ImageTitle { get; set; }
        public string ImageMainText { get; set; }
    }

}